<?php

require_once __DIR__.'/../../../vendor/autoload.php';
require_once 'PHPUnit/Autoload.php';
require_once 'PHPUnit/Framework/Assert/Functions.php';

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;

class AnyClass {
	public $property;

	/**
	 * @return \Workshop\Application\Event\Handler
	 */
	public function getHandler() {
		if(!isset($this->oHandler)) {
			$this->oHandler = new \Workshop\Application\Event\Handler();
		}

		return $this->oHandler;
	}
}

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context, SnippetAcceptingContext
{

	/**
	 * @var AnyClass
	 */
	private $oObject1;
	/**
	 * @var AnyClass
	 */
	private $oObject2;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

	/**
	 * @Given /^I have two objects$/
	 */
	public function iHaveTwoObjects() {
		$this->oObject1 = new AnyClass();
		$this->oObject2 = new AnyClass();
	}

	/**
	 * @Given /^I have two different listeners for event "([^"]*)"$/
	 */
	public function iHaveTwoDifferentListenersForEvent($event) {

		$oObject1 = $this->oObject1;
		$oObject2 = $this->oObject2;

		$oObject1->getHandler()->addEventListener($event, function() use ($oObject1) {
			$oObject1->property = 1;
		});

		$oObject2->getHandler()->addEventListener($event, function() use ($oObject2) {
			$oObject2->property = 2;
		});
	}

	/**
	 * @When /^I raise one event "([^"]*)" on first object$/
	 * @param $event
	 * @throws \Workshop\Application\Event\Exception
	 */
	public function iRaiseOneEventOnFirstObject($event) {
		$aParams = array();
		$this->oObject1->getHandler()->raiseEvent($event, $aParams);
	}

	/**
	 * @Then /^I get only one object listener responding to event$/
	 */
	public function iGetOnlyOneObjectListenerRespondingToEvent() {
		assertTrue($this->oObject1->property == 1);
		assertTrue($this->oObject2->property == null);
	}

	/**
	 * @Given /^Two objects are empty$/
	 */
	public function twoObjectsAreEmpty() {
		assertTrue($this->oObject1->property === null);
		assertTrue($this->oObject2->property === null);
	}

	/**
	 * @Given /^Each object has implemented getHandler method$/
	 */
	public function eachObjectHasImplementedGetHandlerMethod() {
		assertTrue($this->oObject1->getHandler() instanceof \Workshop\Application\Event\Handler);
		assertTrue($this->oObject2->getHandler() instanceof \Workshop\Application\Event\Handler);
	}
}
