Feature: Event handler

  Scenario: Event handler per object
    Given I have two objects
    And Two objects are empty
    And Each object has implemented getHandler method
    And I have two different listeners for event "click"
    When I raise one event "click" on first object
    Then I get only one object listener responding to event
