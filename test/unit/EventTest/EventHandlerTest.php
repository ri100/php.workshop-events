<?php
namespace EventTest {

    use Generi\Boundary\ICollection;
    use Generi\Collection;
	use Workshop\Application\Event\Handler;

	require_once __DIR__ . '/../../../vendor/autoload.php';

    class Raiser extends Handler {

        const EVENT = 'onTestEvent';

	    /**
	     * @var Collection
	     */
        public $oCollection;

        public function raise() {
            $this->oCollection = new Collection();
            $this->raiseEvent(self::EVENT, $this->oCollection);
        }
    }

    class Listener {

        public function onTest(/** @noinspection PhpUnusedParameterInspection */
            $oSender, ICollection $oCollection) {
            $oCollection->add('OK', '\\EventTest\\Listener::onTest');
        }

        public static function onStaticTest(/** @noinspection PhpUnusedParameterInspection */
            $oSender, ICollection $oCollection) {
            $oCollection->add('OK', '\\EventTest\\Listener::onStaticTest');
        }
    }

    function callback(/** @noinspection PhpUnusedParameterInspection */
        $oSender, $oCollection) {
        /* @var $oCollection ICollection */
        $oCollection->add('OK', 'callback');
    }

    /**
     * test case.
     */
    class EventHandlerTest extends \PHPUnit_Framework_TestCase {

        /**
         * Prepares the environment before running a test.
         */
        protected function setUp() {
            parent::setUp();
        }

        /**
         * Cleans up the environment after running a test.
         */
        protected function tearDown() {
            parent::tearDown();
        }

        /**
         * Constructs the test case.
         */
        public function testEventRaise() {
            $oRaiser = new Raiser();
            $oListen = new Listener();

            // Static listener
            $oRaiser->addEventListener(Raiser::EVENT, array('\\EventTest\\Listener', 'onStaticTest'));

            // By instance
            $oRaiser->addEventListener(Raiser::EVENT, array($oListen, 'onTest'));

            // Function
            $oRaiser->addEventListener(Raiser::EVENT, '\\EventTest\\callback');

            // Closure
            /** @noinspection PhpUnusedParameterInspection */
            $oRaiser->addEventListener(Raiser::EVENT, function ($oSender, $oCollection) {
                /* @var $oCollection ICollection */
                $oCollection->add('OK', 'closure');
            });

            $oRaiser->raise();

            $this->assertTrue($oRaiser->oCollection->has('closure'));
            $this->assertTrue($oRaiser->oCollection->has('callback'));
            $this->assertTrue($oRaiser->oCollection->has('\\EventTest\\Listener::onTest'));
            $this->assertTrue($oRaiser->oCollection->has('\\EventTest\\Listener::onStaticTest'));
        }

        public function testRemove() {
            $oRaiser = new Raiser();

            $iEventId = $oRaiser->addEventListener(Raiser::EVENT, '\\EventTest\\callback');
            // Static listener
            $oRaiser->addEventListener(Raiser::EVENT, array('\\EventTest\\Listener', 'onStaticTest'));
            // Remove callback
            $oRaiser->removeEventListener('onTestEvent', $iEventId);
            $oRaiser->raise();

            $this->assertFalse($oRaiser->oCollection->has('\\EventTest\\callback'));
            $this->assertTrue($oRaiser->oCollection->has('\\EventTest\\Listener::onStaticTest'));
        }

        public function testCallbackException() {
            $oRaiser = new Raiser();

            try {
                $oRaiser->addEventListener(Raiser::EVENT, 'NoCallback');
                $oRaiser->raise();
            } catch (\Exception $e) {
                $this->assertInstanceOf('\\Workshop\\Application\\Event\\Exception', $e);
            }
        }

        public function testMethodException() {
            $oRaiser = new Raiser();

            try {
                $oRaiser->addEventListener(Raiser::EVENT, array('Listener', 'NoMethod'));
                $oRaiser->raise();
            } catch (\Exception $e) {
                $this->assertInstanceOf('\\Workshop\\Application\\Event\\Exception', $e);
            }
        }
    }
}

