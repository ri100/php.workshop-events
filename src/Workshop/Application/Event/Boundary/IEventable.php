<?php
namespace Workshop\Application\Event\Boundary;

interface IEventable {
    /**
     * Adds event listener. Listener will listen to $sEvent (on current object) and will
     * call $mListenerCallBack if event occurs.
     *
     * @param    string
     * @param    mixed    Array or string. If array passed must be in a format of correct callback, eg.
     *                    array(&$oObject,'method') or array('class','method').
     *
     * @return    int        Event listener ID
     */
    public function addEventListener($sEvent, $mListenerCallBack);

    /**
     * Removes listener from the raiser object. If you want to remove one example of an obejst not all
     * listeners pass $iEventId. Event Id is returned by addEventListener.
     *
     * @param    string
     * @param    integer
     * @return    void
     */
    public function removeEventListener($sEvent, $iEventId = null);

    /**
     * Raises event and spreads the new about it to all listeners.
     *
     * @param    string
     * @param    mixed
     * @param   IEventable
     * @return    void
     */
    public function raiseEvent($sEvent, $aParams, $oSender = null);

    /**
     * Checks if event is in event queue.
     *
     * @param string $sEvent
     */
    public function hasEventListeners($sEvent);
}