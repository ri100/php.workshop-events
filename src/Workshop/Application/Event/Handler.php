<?php
namespace Workshop\Application\Event {

    use Generi\Boundary\IStringable;
	use Generi\Object;

	/**
     *
     * Class responsible for handling events.
     *
     * Example:
     *
     * <code>
     * class Raiser extends \Application\Event\Handler {
     *    function raise() {
     *        $this->raiseEvent('onTestEvent',array(1));
     *    }
     * }
     *
     * class Listener {
     *    function onTest($oSender,$aParams) {
     *        echo 'onTest';
     *    }
     * }
     *
     * function callback($oSender) {
     *    echo 'callback';
     * }
     *
     * $oRaiser = new Raiser();
     * $oListen = new Listener();
     *
     * // Static listener
     * $oRaiser->addEventListener('onTestEvent',array('Listener','onTest'));
     *
     * // By instance
     * $iEventId = $oRaiser->addEventListener('onTestEvent',array(&$oListen,'onTest'));
     *
     * // Function
     * $oRaiser->addEventListener('onTestEvent','callback');
     *
     * // Closure
     * $oRaiser->addEventListener('onTestEvent',function($oObject, $aParams) { } );
     *
     * // Remove
     * $oRaiser->removeEventListener('onTestEvent',$iEventId);
     *
     * $oRaiser->raise();
     * </code>
     *
     */
    class Handler extends Object implements Boundary\IEventable {

        /**
         * Holds evens and referenced listeners.
         *
         * @var array
         */
        private $aEventListenerObjects = array();
        /**
         * Event counter
         *
         * @var int
         */
        private $iEventCounter = 0;

        final public function __construct() {}

        ///////////////////////////////
        // \IEventable

        /**
         * @param $sEvent
         * @param $mListenerCallBack
         * @return int
         * @throws Exception
         */
        final public function addEventListener($sEvent, $mListenerCallBack) {
            $this->iEventCounter++;
            if ($mListenerCallBack instanceof \Closure) {
                // Closure
                if ($sEvent instanceof IStringable) {
                    $sEvent = $sEvent->__toString();
                }
                $this->aEventListenerObjects[$sEvent][$this->iEventCounter] = array(
                    'closure' => $mListenerCallBack,
                );
            } elseif (is_array($mListenerCallBack) && is_object($mListenerCallBack[0]) && is_string($mListenerCallBack[1])) {
                // Object method
                $this->aEventListenerObjects[$sEvent][$this->iEventCounter] = array(
                    'object' => $mListenerCallBack[0],
                    'method' => $mListenerCallBack[1]
                );
            } elseif (is_array($mListenerCallBack) && is_string($mListenerCallBack[0]) && is_string($mListenerCallBack[1])) {
                // Static method
                $this->aEventListenerObjects[$sEvent][$this->iEventCounter] = array(
                    'class' => $mListenerCallBack[0],
                    'method' => $mListenerCallBack[1]
                );
            } elseif (is_string($mListenerCallBack)) {
                // Function
                $this->aEventListenerObjects[$sEvent][$this->iEventCounter] = array(
                    'callback' => $mListenerCallBack,
                );
            } else {
                throw new Exception('Second parameter must be string or array containing object and method!');
            }

            return $this->iEventCounter;
        }

        /**
         * @param $sEvent
         * @param null $iEventId
         */
        final public function removeEventListener($sEvent, $iEventId = null) {

            if(!is_string($sEvent)) {
                $sEvent = (string) $sEvent;
            }

            if (isset($this->aEventListenerObjects[$sEvent])) {
                if (is_null($iEventId)) {
                    unset($this->aEventListenerObjects[$sEvent]);
                } else {
                    unset($this->aEventListenerObjects[$sEvent][$iEventId]);
                }
            }
        }

        /**
         * @param $sEvent
         * @param $aParams
         * @param null $oSender
         * @throws Exception
         */
        final public function raiseEvent($sEvent, $aParams, $oSender = null) {

            if(!is_string($sEvent)) {
                $sEvent = (string) $sEvent;
            }

            // Is there any listener?
            if ($this->hasEventListeners($sEvent)) {

                if (is_null($oSender)) {
                    $oSender = $this;
                }

                foreach ($this->getEventListeners($sEvent) as $aObjectArray) {
                    if (isset($aObjectArray['closure']) && $aObjectArray['closure'] instanceof \Closure) {
                        // Closure
                        $aObjectArray['closure']($oSender, $aParams);
                    } elseif (isset($aObjectArray['object'])) {
                        // Object
                        $oObject = $aObjectArray['object'];
                        $sMethod = $aObjectArray['method'];

                        if (!method_exists($oObject, $sMethod)) {
                            throw new Exception(
                                sprintf('Method %s->%s is not defined and can not be called during event: %s!',
                                    $oObject,
                                    $sMethod,
                                    get_class($this) . $sEvent
                                )
                            );
                        }
                        $oObject->$sMethod($oSender, $aParams);
                    } elseif (isset($aObjectArray['class'])) {
                        // TODO: test if &this works, noe deleted
                        if (!method_exists($aObjectArray['class'], $aObjectArray['method'])) {
                            throw new Exception(
                                sprintf(
                                    'Method %s::%s is not defined and can not be called during event: %s!',
                                    $aObjectArray['class'],
                                    $aObjectArray['method'],
                                    get_class($this) . $sEvent
                                )
                            );
                        }
                        call_user_func(array($aObjectArray['class'], $aObjectArray['method']), $oSender, $aParams);
                    } elseif (function_exists($aObjectArray['callback'])) {
                        // Function
                        // TODO: test if &this works
                        call_user_func($aObjectArray['callback'], $oSender, $aParams);
                    } else {
                        throw new Exception(
                            sprintf(
                                'Function %s does not exist and can not be called during event: %s!',
                                $aObjectArray['callback'],
                                get_class($this) . $sEvent
                            )
                        );
                    }
                }
            }
        }

        /**
         * @param string $sEvent
         * @return bool
         */
        final public function hasEventListeners($sEvent) {

            if(!is_string($sEvent)) {
                $sEvent = (string) $sEvent;
            }

            return isset($this->aEventListenerObjects[$sEvent]);
        }

        /**
         * @param $sEvent
         * @return array
         */
        final public function getEventListeners($sEvent) {

            if(!is_string($sEvent)) {
                $sEvent = (string) $sEvent;
            }

            return $this->aEventListenerObjects[$sEvent];
        }

        /**
         * @return boolean
         */
        final public function hasEvents() {
            return !empty($this->aEventListenerObjects);
        }

        /**
         * @return array
         */
        final public function getEvents() {
            return $this->aEventListenerObjects;
        }

    }
}
